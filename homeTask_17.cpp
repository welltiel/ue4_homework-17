﻿#include <iostream>
#include <math.h>

class Vector
{
private:
	double x;
	double y;
	double z;

public:
	Vector() : x(1), y(1), z(1)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void ShowParams()
	{
		std::cout << x << ' ' << y << ' ' << z << "\n";
	}
	double CalculateVectorLength()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
};

int main()
{
	Vector temp, temp2(1, 2, 3);
	temp.ShowParams();
	temp2.ShowParams();
	std::cout << "Length of default vector is: " << temp.CalculateVectorLength() << "\n";
	std::cout << "Length of my vector is: " << temp2.CalculateVectorLength();
}

